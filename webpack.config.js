var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require("webpack");
var SvgStore = require('webpack-svgstore-plugin');

var production = process.env.NODE_ENV === 'production';

var plugins = [
  new ExtractTextPlugin('main.css', {allChunks: true}),
  new HtmlWebpackPlugin({
      template: './src/index.html',
      inject: 'body',
      minify: false
  }),
  new webpack.ProvidePlugin({
      "window.jQuery": "jquery"
  }),
  new SvgStore({
    // svgo options
    svgoOptions: {
      plugins: [
        { removeTitle: true }
      ]
    },
    prefix: 'icon__'
  })   
];

module.exports = {
  entry: {
    app: ['./src/js/app.js']
  },
  output: {
    path: './dist',
    filename: '[name].bundle.js',
    publicPath: ''
  },
  module: {
    loaders: [{
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract(
        require.resolve('style-loader'),
        require.resolve('css-loader') + '!' + require.resolve('sass-loader')
      )
    }, {
      test: /\.css$/,
      loaders: [require.resolve('css-loader')]
    }, {
      test: /\.html$/,
      loaders: [require.resolve('html-loader')]
    }, {
      test: /\.(ttf|eot|otf|woff(2)?)(\?[a-z0-9]+)?$/,
      loader: 'file-loader'
    }, {
        test: /\.json$/,
        loader: require.resolve('json-loader')
    }, {
      test: /\.(png|gif|jpe?g|svg)$/i,
      loader: 'url-loader',
      query: {
        limit: 10000
      }
    }, {
      test: /.js$/,
      loader: require.resolve('babel-loader'),
      exclude: /node_modules/,
      query: {
        presets: [require.resolve('babel-preset-es2015')]
      }
    }]
  },

  sassLoader: {
      includePaths: [path.resolve(__dirname, './src/style')]
  },

  devServer: {
    hot: true
  },
  debug: !production,
  devtool: production ? false : 'eval',
  plugins: plugins
};
