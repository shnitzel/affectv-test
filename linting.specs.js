var linters = require('./dev/linting/linters');
var lintingConfigurationBuilder = require('./dev/linting/linting.config.builder');

var lintingConfigurations = lintingConfigurationBuilder(linters.ESLINT).build();

module.exports = lintingConfigurations.specs;
