var path = require('path');
var webpack = require('webpack');

module.exports = {
  output: {},
  devtool: 'eval',
  debug: true,
  entry: {},
  progress: false,

  module: {
    loaders: [{
      test: /\.html$/,
      loaders: [require.resolve('html-loader')]
    }, {
      test: /\.json$/,
      loader: require.resolve('json-loader')
    }, {
      test: /\.svg$/,
      loaders: [require.resolve('svg-url-loader') + '?noquotes', require.resolve('svgo-loader')]
    }, {
      test: /.js$/,
      loader: require.resolve('babel-loader'),
      exclude: /node_modules/,
      query: {
        presets: [require.resolve('babel-preset-es2015')]
      }
    }]
  },

  plugins: [
    new webpack.ProvidePlugin({})
  ],
  resolveLoader: {
    root: [
      path.join(__dirname, 'node_modules')
    ]
  }
};
