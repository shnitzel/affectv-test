// import Basics
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
// import ngMaterial from 'angular-material';

// Sass
import 'normalize.css';
import '../style/index.scss';

// Load Directives / Components
import CoreComponent from './components/core';
import GeneratedAdComponent from './components/generatedAd';


angular
  .module('app', [uiRouter, uiBootstrap, 'ngMaterial', CoreComponent.name, GeneratedAdComponent.name])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('core', {
        url: '/',
        component: 'affectvCore'
      })
  
      $urlRouterProvider.otherwise('/');
  }]);


