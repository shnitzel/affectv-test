import generatedAdController from './generatedAd.controller';
import generatedAdTemplate from './generatedAd.template.html';

const generatedAdComponent = {
  ngName: 'generatedAd',
  controllerAs: 'vm',
  bindings: {
  	data: '<'
  },
  controller: generatedAdController,
  template: generatedAdTemplate
};

export default generatedAdComponent;
