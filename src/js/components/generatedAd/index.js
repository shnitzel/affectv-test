import generatedAdComponent from './generatedAd.component';

module.exports = angular.module('generatedAd', [])
  .component(generatedAdComponent.ngName, generatedAdComponent);
