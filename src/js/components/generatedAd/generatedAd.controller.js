GeneratedAdController.$inject = ['$scope', '$timeout', 'coreService'];

function GeneratedAdController ($scope, $timeout, coreService) {

  const vm = this;

  $scope.$on('update', function(){
    coreService.getProductInfo()
    .then((products) => {
      vm.data = products;
      $timeout(function(){
        var generatedProducts = document.getElementById("ad-markup").innerHTML;
        vm.updateTextarea(generatedProducts); 
      });
    });
  });  

  vm.updateTextarea = function(param) {
      vm.compiledMarkUp = `
      <!DOCTYPE html>
      <html>
        <head lang="en">
          <meta charset="UTF-8">
          <title></title>
          <style>
            html,
            body {
              font-family: Sans-Serif;
              background-color: #fff;
              height: 100%;
              margin: 0;
              padding: 0;
            }
            .ad-product {
              padding: 40px;
            }  
            
            .ad-product__img {
              padding-bottom: 40px;
            }    
            .ad-product__img img {
              width: 100%;
            }
            
            .ad-product__infoBox {
              text-align: center;
              border: 1px solid #cecece;
              padding: 20px;
            }
            h1 {
              margin: 0;
              padding: 0 0 20px 0;
              font-size: 18px;
              line-height: 24px;
            }
        
            a {
              display: inline-block;
              border-radius: 3px;
              text-decoration: none;
              padding: 10px;
            }
          </style>
        </head>
        <body>
          ` + param + `
        </body>
      </html>`;
  };

} 

export default GeneratedAdController;
