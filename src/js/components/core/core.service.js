CoreService.ngName = 'coreService';

CoreService.$inject = ['$http', '$q', '$filter'];

function CoreService($http, $q, $filter) {

  return {
    getProductInfo: getProductInfo,
    updateProductInfo: updateProductInfo
  };

  function getProductInfo() {
    return $http({ method: 'GET', url: 'http://localhost:3000/products'})
      .then(({data}) => {
        return data;
      });
  }

  function updateProductInfo(param) {
    return fetch('http://localhost:3000/products/1', {
      method: 'put',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(param)
    }).then(function(response) {
      return response.json()
    }).then(function(json) {
      console.log('parsed json: ', json)
    }).catch(function(ex) {
      console.log('parsing failed: ', ex)
    });
  }
}

export default CoreService;
