import coreComponent from './core.component';
import coreService from './core.service';

module.exports = angular.module('core', [])
  .component(coreComponent.ngName, coreComponent)
  .service(coreService.ngName, coreService);
  