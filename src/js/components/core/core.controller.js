const brandImage = require('../../../assets/img/affectv-logo.png');

CoreController.$inject = ['$timeout', '$scope', '$rootScope', '$compile', '$log', 'coreService'];

function CoreController($timeout, $scope, $rootScope, $compile, $log, coreService) {

  const vm = this;
  
  vm.brandImage = brandImage;
  vm.title = 'Dynamic Ad Creator';
  vm.products = [];

  $scope.broadcast = function(){
    $scope.$broadcast('update');
  };

  coreService.getProductInfo()
  .then((products) => {
    vm.products = products;
  });

  vm.generateAd = function() {      
    coreService.updateProductInfo(vm.products[0]).then((products) => {
      $scope.broadcast();
    });
  };
}

export default CoreController;
