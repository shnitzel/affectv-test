import coreController from './core.controller';
import coreTemplate from './core.template.html';

const coreComponent = {
  ngName: 'affectvCore',
  controllerAs: 'vm',
  bindings: {},
  controller: coreController,
  template: coreTemplate
};

export default coreComponent;
