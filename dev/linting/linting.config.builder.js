'use strict';

var linters = require('./linters');
var isUndefined = require('lodash/isUndefined');

function lintingConfigurationBuilder(linter) {

    if(isUndefined(linter)) {
        throw new Error('linter type must be provided');
    }

    var defaultConfig;

    init();

    return {
        build: build
    };

    function init() {
        defaultConfig = linters.ESLINT.config;
    }

    function build() {
        return defaultConfig;
    }

}

module.exports = lintingConfigurationBuilder;
