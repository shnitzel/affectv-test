'use strict';

var path = require('path');

var eslintSpecConfig = {
    'extends': path.resolve(__dirname, 'eslint.base.config.js'),
    'globals': {
        'jasmine': false,
        'describe': false,
        'xdescribe': false,
        'it': false,
        'xit': false,
        'expect': false,
        'beforeEach': false,
        'afterEach': false,
        'spyOn': false,
        'fail': false,
        'TestUtils': false
    },
    'rules': {
        // angular 1 rules
        'angular/document-service': 0, // deactivate for tests
        'angular/window-service': 0, // deactivate for tests
        'angular/timeout-service': 0, // deactivate for tests
        'angular/json-functions': 0, // deactivate for tests
    }
};

module.exports = eslintSpecConfig;
