'use strict';

var eslintConfig = {
    'extends': 'angular',
    'env': {
        'browser': true,
        'node': true,
        'commonjs': true,
        'protractor': true,
        'jquery': true
    },
    'parserOptions': {
        'ecmaVersion': 6,
        'sourceType': 'module'
    },
    'globals': {
        'globalConfig': true,
        '__webpack_public_path__': false
    },
    'rules': {
        'no-console': 2,
        'block-scoped-var': 2,
        'camelcase': 2,
        'curly': [2, 'all'],
        'dot-notation': [2, {'allowKeywords': true}],
        'eqeqeq': [2, 'allow-null'],
        'guard-for-in': 2,
        'new-cap': 2,
        'no-bitwise': 2,
        'no-caller': 2,
        'no-cond-assign': [2, 'except-parens'],
        'no-debugger': 2,
        'no-empty': 2,
        'no-eval': 2,
        'no-extend-native': 2,
        'no-irregular-whitespace': 2,
        'no-iterator': 2,
        'no-loop-func': 2,
        'no-multi-str': 2,
        'no-new': 2,
        'no-proto': 2,
        'no-script-url': 2,
        'no-sequences': 2,
        'no-shadow': 2,
        'no-undef': 2,
        'no-unused-vars': 2,
        'no-with': 2,
        'quotes': [2, 'single'],
        'semi': [0, 'never'],
        'valid-typeof': 2,
        'wrap-iife': [2, 'inside'],
        // angular 1 rules
        'angular/module-setter': 0, // Exported angular modules are treated as an angular modules saved in a variable
        'angular/no-service-method': 0, // use factory instead of service is wrong and is disabled -> angular recommends only to use service
        'angular/no-private-call': 0 // we are using $$properties in it makes sense to declare this object component specific
    }
};

module.exports = eslintConfig;
