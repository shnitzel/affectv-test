'use strict';

var eslintBaseConfig = require('./eslint/eslint.base.config');
var eslintSpecsConfig = require('./eslint/eslint.specs.config');

var linters = {
    'ESLINT': {
        config: {
            base: eslintBaseConfig,
            specs: eslintSpecsConfig
        }
    }
};

module.exports = linters;
