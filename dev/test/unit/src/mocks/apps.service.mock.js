import angular from 'angular';

let AppsServiceMock = () => {
  return jasmine.createSpyObj('appService', ['getApps']);
}

module.exports = angular.module('bcc.apps.service.mock', [])
  .service('appService', AppsServiceMock);
