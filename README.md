# Test

## Requirements

``` bash
npm i

npm install -g json-server

```

## Build & development

**Mock API**
``` bash
json-server --watch src/mock/db.json
```

Open http://localhost:3000 in your browser.


**Develop**
``` bash
npm run serve
```

Open http://localhost:8080 in your browser.

**Export deployable**
``` bash
npm run build
```

